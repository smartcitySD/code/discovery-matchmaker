import json
import time
import os
import math
import copy
import logic.FeedbackManager as feedbackManager
import logic.Planner as planner

from entities.Plan import Plan
from entities.SimpleStep import SimpleStep
from entities.SequentialStep import SequentialStep
from entities.ParallelStep import ParallelStep
from entities.PlanGraph import PlanGraph
from entities.Vertex import Vertex
from entities.Edge import Edge


from random import choice
from pprint import pprint
from pymongo import MongoClient
from owlready2 import *
from numpy import number
from xml.dom import minidom

"""Mongo DB"""
client = MongoClient('localhost', 27017)
db = client.surf
services = db.services
allServices = services.find()
servicesList = []
for service in allServices:
    servicesList.append(service)  

"""
This function randomly selects a number of queries and extract the offers that can get as responses
from the OWL-S data set definition
@queries: Number of queries to select
"""    
def selectAtomicQueries(queries):
    requestFileNames = []
    i = 0
    doc = minidom.parse("../../Data/services-dataset/OWLS-TC4_SWRL/owls-tc4.xml")
    relevanceSet = doc.getElementsByTagName("binaryrelevanceset")[0]
    requests = relevanceSet.getElementsByTagName("request")
    responses = {}
    with open("../../Data/requests-dataset/json-responses/responses_1.json") as dataFile:    
        responses = json.load(dataFile)
    while i < queries:
        requestFileName = choice(os.listdir("../../Data/services-dataset/OWLS-TC4_SWRL/htdocs/queries/1.1/"))
        if requestFileName not in requestFileNames:
            requestFileName = requestFileName.replace(".owls", ".json")
            service = {}
            try:
                """ Defining request """
                with open("../../Data/services-dataset/json-services/all/" + requestFileName) as dataFile:    
                    service = json.load(dataFile)
                request = {}
                request["id"] = service["name"] + "_request"
                request["domains"] = service["domains"]
                request["inputs"] = service["inputs"]
                request["outputs"] = service["outputs"]
                
                """ Defining responses """
                response = {}
                print("Defining responses for: " + service["name"] + " atomic service.")
                requestFileName = requestFileName.replace(".json", ".owls")
                for req in requests:
                    uri = req.getElementsByTagName("uri")[0].firstChild.data
                    if requestFileName in uri:
                        response["request"] = uri
                        response["type"] = "atomic"
                        ratings = req.getElementsByTagName("ratings")[0]
                        offers = ratings.getElementsByTagName("offer")
                        offersArray = []
                        for offer in offers:
                            offerElement = {}
                            uriOffer = offer.getElementsByTagName("uri")[0].firstChild.data
                            relevant = offer.getElementsByTagName("relevant")[0].firstChild.data
                            uriOffer = uriOffer.replace("http://127.0.0.1/services/1.1/","")
                            fileName = uriOffer.split("#")[0]
                            fileName = fileName.replace(".owls",".json")
                            with open("../../Data/services-dataset/json-services/all/" + fileName) as dataFile:    
                                s = json.load(dataFile)
                            offerElement["id"] = s["id"]
                            offerElement["offer"] = uriOffer
                            offerElement["relevant"] = relevant
                            offersArray.append(offerElement)
                        response["offers"] = offersArray
                
                """ Writing files if responses exist"""        
                if response != {}:
                    responses[service["name"]] = response
                    """ Writing responses file with new response"""
                    with open("../../Data/requests-dataset/json-responses/responses_1.json", "w") as outfile:
                        json.dump(responses, outfile, indent=3)
                
                    """ Writing request"""
                    os.makedirs(os.path.dirname("../../Data/requests-dataset/json-requests/1/" + request["id"] + ".json"), exist_ok=True)
                    with open("../../Data/requests-dataset/json-requests/1/" + request["id"] + ".json", "w") as outfile2:
                        json.dump(request, outfile2, indent=3)
                    i = i + 1
                    print("Request Created: " + str(i))
                    requestFileNames.append(requestFileName)
            except:
                print("Trying again...")
                
                
"""
This function randomly selects a number of requests for atomic and composed services and determines the offers that can get as responses
using the algorithm to define the requests
@servicesNum: Number of services in the composed plan
@queries: Number of queries to select
"""    
def selectComposedQueries(servicesNum, queries):
    i = 0
    responses = {}
    with open("../../Data/requests-dataset/json-responses/responses_"+str(servicesNum)+".json") as dataFile:    
        responses = json.load(dataFile)
    while i < queries:
        requestFileNames = []
        if servicesNum >= 1:
            service1 = choice(servicesList)
            service2 = choice(servicesList)
            domainsService1 = []
            for domain in service1["domains"]:
                if domain not in domainsService1:
                    domainsService1.append(domain)
            domainsService2 = []
            for domain in service2["domains"]:
                if domain not in domainsService2:
                    domainsService2.append(domain)
            
            domainsRelation = False
            for domain in domainsService1:
                if domain in domainsService2:
                    domainsRelation = True
                    break
            
            
            if(service1["name"]!=service2["name"] and domainsRelation):
                sn = service1["name"]+"_"+service2["name"]
                if sn not in requestFileNames:
                    try:
                        request = {}
                        request["id"] = service1["name"] + "_" + service2["name"] + "_request"
                        domains = []
                        for domain in service1["domains"]:
                            if domain not in domains:
                                domains.append(domain)
                        for domain in service2["domains"]:
                            if domain not in domains:
                                domains.append(domain)
                        request["domains"] = domains
                        request["inputs"] = service1["inputs"]
                        request["outputs"] = service2["outputs"]
                        """ Defining responses """
                        response = {}
                        start = int(round(time.time() * 1000))
                        print("Defining responses for: " + request["id"] + " with " + str(servicesNum) + " service(s) at " + str(start)+".") 
                        plans = discoverPlans(request, {}, servicesNum, start)
                        if len(plans) > 0:
                            response["request"] = request["id"]
                            response["type"] = "composed"
                            offersArray = []
                            for k, plan in plans.items():
                                if plan.getState() == 1:
                                    offer = {}
                                    graph = []
                                    for edge in plan.getEdges():
                                        link = ""
                                        link = "from " + edge.getSource().getName() + " to " + edge.getDestination().getName()
                                        graph.append(link) 
                                    offer["graph"] = graph
                                    requestDomain = ""
                                    for domain in request["domains"]:
                                        requestDomain = domain
                                    if requestDomain in plan.getDomains():
                                        offer["relevant"] = 1
                                    else:
                                        offer["relevant"] = 0
                                    offersArray.append(offer)
                            response["offers"] = offersArray
                
                        """ Writing files if responses exist"""        
                        if response != {}:
                            responses[service1["name"] + "_" + service2["name"]] = response
                            """ Writing responses file with new response"""
                            with open("../../Data/requests-dataset/json-responses/responses_"+str(servicesNum)+".json", "w") as outfile:
                                json.dump(responses, outfile, indent=3)
                
                            """ Writing request"""
                            os.makedirs(os.path.dirname("../../Data/requests-dataset/json-requests/" + str(servicesNum) + "/" + request["id"] + ".json"), exist_ok=True)
                            with open("../../Data/requests-dataset/json-requests/" + str(servicesNum) + "/" + request["id"] + ".json", "w") as outfile:
                                json.dump(request, outfile, indent=3)
                            i = i + 1
                            print("Request Created: " + str(i))
                            requestFileNames.append(service1["name"]+"_"+service2["name"])
                    except:
                        print("Trying again...")
                        raise
        else:
            print("Number of services should be bigger than 1")
            break

def discoverPlans(jsonRequest, previousPlans, numberOfServices,start):
    current = int(round(time.time() * 1000))
    if (current - start) >= 120000:
        print("Timeout trying with next pair of services...")
        return {}
    returnPlans = False
    matchingType = 0
    similarityThreshold = 0.5
    feedbackThreshold = 0.5
    plans = {}
    """ Create new plan or use previous"""
    if(len(previousPlans) == 0):
        plan = PlanGraph(0, {}, [], 0.0, [], [], [])
        initial = Vertex(0, "initial", 1, {}, [], [], jsonRequest["inputs"][:], jsonRequest["domains"][:])
        initial.setState(1);
        plan.getVertexs()[initial.getName()] = initial
        final = Vertex(1, "final", 0, {}, jsonRequest["outputs"][:], jsonRequest["outputs"][:], [], jsonRequest["domains"][:])
        final.setRemainingInputs(jsonRequest["outputs"][:])
        plan.getVertexs()[final.getName()] = final
        plan.setInputs(jsonRequest["inputs"][:])
        plan.setOutputs(jsonRequest["outputs"][:])
        plan.setCurrentInputs(jsonRequest["outputs"][:])
        plan.setState(0)
        for domain in jsonRequest["domains"]:
            if domain not in plan.getDomains():
                plan.getDomains().append(domain)
        previousPlans[len(previousPlans)] = plan
        
    """Graph creation"""
    for kp, plan in previousPlans.items():
        requestOutputs = plan.getCurrentInputs()[:]
        requestInputs = plan.getInputs()[:]
        """Defining search space"""
        parametersTypes = getParameterTypes(requestOutputs)
        searchSpace = []
        servicesNames = []
        # searchSpace = services.find({ "outputs.type": {"$in" : parametersTypes } })
        for parameterType in parametersTypes:
            servicesList = services.find({ "outputs.type": parameterType})
            for service in servicesList:
                add = False
                for d in service["domains"]:
                    if d in jsonRequest["domains"]:
                        add = True
                        break
                if service["name"] not in servicesNames and add:
                    searchSpace.append(service)
                    servicesNames.append(service["name"])
            
        if len(searchSpace) == 0:
            servicesList = services.find()
            for service in servicesList:
                if service["name"] not in servicesNames:
                    searchSpace.append(service)
                    servicesNames.append(service["name"])
        
        """Compare each output of each service with each remaining input on each remaining vertex"""
        for service in searchSpace:
            tempPlan = copy.deepcopy(plan)
            addTemp = False
            discoveredVertex = Vertex(-1, "", 0, {}, [], [], [], [])
            if service["name"] in tempPlan.getVertexs().keys():
                discoveredVertex = tempPlan.getVertexs()[service["name"]]
            else:
                discoveredVertex.setType(2)
                discoveredVertex.setName(service["name"])
                discoveredVertex.setState(0)
                discoveredVertex.setService(service)
                discoveredVertex.setInputs(service["inputs"][:])
                discoveredVertex.setRemainingInputs(service["inputs"][:])
                discoveredVertex.setOutputs(service["outputs"][:])
                discoveredVertex.setDomains(service["domains"][:])
            remainingVertexs = tempPlan.getRemainingVertexs()
            comparison = planner.hybridMatchmaker2(remainingVertexs, discoveredVertex, matchingType, similarityThreshold)        
            if comparison["match"] == True:
                edges = comparison["edges"]
                for edge in edges:
                    link = edge.getLink()
                    tempPlan.getVertexs()[service["name"]]=discoveredVertex
                    tempPlan.update(edge.getDestination(),edge.getSource(),link["matchedInput"],link["matchedOutput"])
                    tempPlan.addEdge(edge)
                    for domain in service["domains"]:
                        if domain not in tempPlan.getDomains():
                            tempPlan.getDomains().append(domain)
                    addTemp = True
            if addTemp:
                initial = tempPlan.getVertexs()["initial"]
                init = False
                remainingVertexsTemp = tempPlan.getRemainingVertexs()
                comparison = planner.hybridMatchmaker2(remainingVertexsTemp, initial, matchingType, similarityThreshold)        
                if comparison["match"] == True:
                    init = True
                    edges = comparison["edges"]
                    for edge in edges:
                        link = edge.getLink()
                        tempPlan.update(edge.getDestination(),edge.getSource(),link["matchedInput"],link["matchedOutput"])
                        tempPlan.addEdge(edge)
                    for d in service["domains"]:
                        if d not in tempPlan.getDomains():
                            tempPlan.getDomains().append(d)
                if len(tempPlan.getRemainingVertexs()) == 0 and init == True:
                    tempPlan.setState(1)
                    plans[len(plans)] = copy.deepcopy(tempPlan)
                    if (len(tempPlan.getVertexs()) - 2) == numberOfServices:
                        returnPlans = True
                else:
                    if (len(tempPlan.getVertexs()) - 2) < numberOfServices:
                        tempPlans = {}
                        tempPlans[len(tempPlans)] = copy.deepcopy(tempPlan)
                        tempPlans = discoverPlans(jsonRequest, tempPlans, numberOfServices, start)
                        for k3, p in tempPlans.items():
                            if p.getState() == 1:
                                plans[len(plans)] = copy.deepcopy(p)
                                if (len(p.getVertexs()) - 2) == numberOfServices:
                                    returnPlans = True
    if returnPlans:
        return plans
    else:
        return {}
    
"""
Get super classes and sub classes of a list of parameters
@parameters: List of parameters to determine superClasses and sub classes.
@return List of super and sub classes.
"""
def getParameterTypes(parameters):
    parameterTypes = []
    for parameter in parameters:
        parameterType = parameter["type"]
        if parameterType not in parameterTypes:
            parameterTypes.append(parameterType)
        ontology = parameterType.split("#")
        if(ontology[0] != "http://127.0.0.1/ontology/protont.owl" and ontology[0] != "http://127.0.0.1/ontology/protonu.owl" and ontology[0] != "http://127.0.0.1/ontology/protons.owl"):
            onto = get_ontology(ontology[0])
            onto.load()
            parameterClasses = onto.search(iri=parameterType)
            for parameterClass in parameterClasses:
                subconcepts = onto.search(subclass_of=parameterClass) 
                for subconcept in subconcepts:
                    if subconcept not in parameterTypes:
                        parameterTypes.append(subconcept.iri)
                superconcepts = parameterClass.is_a
                for superconcept in superconcepts:
                    if superconcept not in parameterTypes and str(type(superconcept)) == "<class 'owlready2.entity.ThingClass'>" :
                        parameterTypes.append(superconcept.iri)
    return parameterTypes

"""
Create conversation approach requests
"""
def createConversationsRequests():
    folders = os.listdir("../../Data/requests-dataset/json-requests/")
    for folder in folders:
        files = os.listdir("../../Data/requests-dataset/json-requests/"+folder+"/")
        if folder == '1':
            for file in files:
                conversationRequest = {}
                request = {}
                with open("../../Data/requests-dataset/json-requests/" + folder + "/" + file) as dataFile:    
                    request = json.load(dataFile)
                conversationRequest["id"] = request["id"]
                conversationRequest["domains"] = request["domains"]
                tasks = []
                task = {}
                task["name"] = "task-1"
                task["inputs"] = request["inputs"]
                task["outputs"] = request["outputs"]
                task["services"] = []
                tasks.append(task)
                conversationRequest["tasks"] = tasks
                """ Writing conversation request"""
                os.makedirs(os.path.dirname("../../Data/requests-dataset/conversations-request/" + folder + "/" + request["id"] + ".json"), exist_ok=True)
                with open("../../Data/requests-dataset/conversations-request/" + folder + "/" + request["id"] + ".json", "w") as outfile:
                    json.dump(conversationRequest, outfile, indent=3)
                print("New converation request created: " + request["id"])
        else:
            with open("../../Data/requests-dataset/json-responses/responses_"+folder+".json") as dataFile:    
                    responses = json.load(dataFile)
            for k, response in responses.items():
                conversationFound = False
                request = response["request"]
                offers = response["offers"]
                for offer in offers:
                    services = []
                    graph = offer["graph"]
                    for line in graph:
                        ss = line.split(" to ")
                        for s in ss:
                            s = s.replace("from","")
                            if s.replace(" ","") != "initial" and s.replace(" ","") != "final":
                                found = False
                                for ser in services:
                                    if ser.replace(" ","") == s.replace(" ",""):
                                        found = True
                                        break
                                if not found:
                                    services.append(s)
                    if len(services) == int(folder):
                        try:
                            with open("../../Data/requests-dataset/json-requests/"+folder+"/"+request+".json") as dataFile:    
                                originalRequest = json.load(dataFile)
                            conversationRequest["id"] = originalRequest["id"]
                            conversationRequest["domains"] = originalRequest["domains"]
                            conversationRequest["inputs"] = originalRequest["inputs"]
                            conversationRequest["outputs"] = originalRequest["outputs"]
                            tasks = []
                            t = 1
                            for service in services:
                                files = os.listdir("../../Data/services-dataset/all")
                                for file in files:
                                    with open("../../Data/services-dataset/all/" + file) as dataFile:    
                                        serviceFile = json.load(dataFile)
                                    if serviceFile["name"].replace(" ","") == service.replace(" ",""):
                                        task = {}
                                        task["name"] = "task-"+str(t)
                                        task["inputs"] = serviceFile["inputs"]
                                        task["outputs"] = serviceFile["outputs"]
                                        task["services"] = []
                                        tasks.append(task)
                                        t = t + 1
                                        break
                            if len(tasks) == int(folder):
                                conversationRequest["tasks"] = tasks
                                """ Writing conversation request"""
                                os.makedirs(os.path.dirname("../../Data/requests-dataset/conversations-request/" + folder + "/" + request + ".json"), exist_ok=True)
                                with open("../../Data/requests-dataset/conversations-request/" + folder + "/" + request + ".json", "w") as outfile:
                                    json.dump(conversationRequest, outfile, indent=3)
                                print("New conversation request created: " + request)
                                conversationFound = True
                        except:
                            print("Error finding original request file")
                    if conversationFound:
                        break
                
                                       
                
                
                
