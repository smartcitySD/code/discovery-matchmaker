"""
Service Discovery Engine
This project implements two algorithms to discover atomic services and composition plans
    of atomic services using signature matchmaking:typeService
    1. Hybrid IO matchmaker.
    2. SURF matchmaker.

Author: Christian Cabrera
"""
import logic.Planner as planner
import logic.DataGenerator as dataGenerator
import logic.FeedbackManager as feedbackManager
import paho.mqtt.client as mqttClient
import json
import time
import os
from entities.PlanGraph import PlanGraph
from plainbox.impl import job
gatewayNumber = 1
subscriberConnected = False #global variable for the state of the connection
publisherConnected = False #global variable for the state of the connection

lastGateway = True
gatewayAddress= "localhost"
#gatewayAddress= "192.168.3.2"
nextAddress= "192.168.3.3"
port = 1883

def on_publisher_connect(client, userdata, flags, rc):
    if rc == 0:
        #print("Connected to broker, ready to publish messages...")
        global publisherConnected                
        publisherConnected = True                
    else:
        print("Connection failed")

def publishMessage(broker_address,port,topic,message):
    publisher = mqttClient.Client()               
    publisher.on_connect= on_publisher_connect               
    publisher.connect(broker_address, port=port)
    publisher.loop_start()
    while publisherConnected != True:    
        time.sleep(0.1)
    publisher.publish(topic, payload=message)
    #print("Message published: " + topic)
    time.sleep(0.5)
    publisher.disconnect()
    publisher.loop_stop()
    global publisherConnected                
    publisherConnected = False
   

def on_subscriber_connect(client, userdata, flags, rc):
    if rc == 0:
         #print("Connected to broker, waiting for messages...")
         global subscriberConnected                
         subscriberConnected = True               
    else:
        print("Connection failed")
         
def on_message_received(client, userdata, message):
    client.disconnect()
    client.loop_stop()
    global subscriberConnected 
    subscriberConnected = False
    receivedTime = int(round(time.time() * 1000))
    message = str(message.payload).replace("b'", "").replace("'","").replace("\\","")
    #print(message)
    jsonMessage=json.loads(message)
    messageType = jsonMessage["messageType"]
    if messageType == "request":
        jobId = jsonMessage["jobId"]
        consumer = jsonMessage["consumerAddress"]
        print("New Request Received: " + jobId)
        jsonPlans = {} 
        discoveryProcess(jsonMessage,jsonPlans,consumer)
    if messageType == "forward":
        jobId = jsonMessage["jobId"]
        jsonRequest = jsonMessage["jsonRequest"]
        consumer = jsonRequest["consumerAddress"]
        print("New Request Forwarded Received: " + jobId)
        jsonPlans = {} 
        discoveryProcess(jsonRequest,jsonPlans,consumer)
    if messageType  == "feedback":
        jobId = jsonMessage["jobId"]
        consumer = jsonMessage["consumerAddress"]
        print("New Feedback Received: " + jobId)
        plans = jsonMessage["plans"]
        for plan in plans:
            planId = plan["planId"]
            feedback = plan["feedback"]
            feedbackManager.updateFeedback(jobId,planId,feedback)
        print("Feedback ACK: " + jobId)
        jsonResponse = {}
        jsonResponse["messageType"] = "feedbackACK"
        message = json.dumps(jsonResponse)
        print("\n***** Waiting for new request *****")
        subscribeToMessage(gatewayAddress,"/discovery/request")
        publishMessage(consumer,port,"/discovery/feedbackACK/"+jobId,message)
    if messageType  == "remove":
        consumer = jsonMessage["consumerAddress"]
        feedbackManager.clean()
        jsonResponse = {}
        jsonResponse["messageType"] = "removeACK"
        message = json.dumps(jsonResponse)
        print("\n***** Waiting for new request *****")
        subscribeToMessage(gatewayAddress,"/discovery/request")
        publishMessage(consumer,port,"/discovery/removeACK/",message)
    if messageType  == "removeServices":
        consumer = jsonMessage["consumerAddress"]
        servicesPerGateway = jsonMessage["servicesPerGateway"]
        feedbackManager.updateServiceCollection(servicesPerGateway,gatewayNumber)
        jsonResponse = {}
        jsonResponse["messageType"] = "removeServicesACK"
        message = json.dumps(jsonResponse)
        print("\n***** Waiting for new request *****")
        subscribeToMessage(gatewayAddress,"/discovery/request")
        publishMessage(consumer,port,"/discovery/removeServicesACK/",message)
        
def subscribeToMessage(broker_address,topic):
    subscriber = mqttClient.Client()               
    subscriber.on_connect= on_subscriber_connect                      
    subscriber.on_message= on_message_received
    subscriber.connect(broker_address, port=port)
    subscriber.loop_start()
    while subscriberConnected != True:    
        time.sleep(0.1)
    subscriber.subscribe(topic)
    #print("Subscribed to: " + topic)

def discoveryProcess(jsonRequest,plans,consumer):
    jsonResponse = {}
    discoveryStartTime = int(round(time.time() * 1000))
    print("Starting Discovery Process")
    #print("Request: " + json.dumps(jsonRequest, indent=3))
    requestTime = jsonRequest["requestTime"]
    jobId = jsonRequest["jobId"]
    similarityThreshold = jsonRequest["similarityThreshold"]
    """ Choose algorithm to use according to request """
    discoveryType = jsonRequest["discoveryType"]
    graphsPlans = {}
    tempPlans = {}
    if discoveryType <= 4:
        try:
            graphsPlans["plans"] = planner.backwardPlanningClassic(jsonRequest,plans,discoveryType,1.0,int(jsonRequest["services"]))
        except:
            print ("exception in the planner")
        
        #graphsPlans["plans"] = planner.backwardPlanningClassic2(jsonRequest,plans,discoveryType,similarityThreshold,int(jsonRequest["services"]))
    elif discoveryType<=9:
        feedbackThreshold = jsonRequest["feedbackThreshold"]
        functionalThreshold = jsonRequest["functionalThreshold"]
        topK = jsonRequest["topK"]
        disType = 0
        if discoveryType == 5:
            disType = 0
        if discoveryType == 6:
            disType = 1
        if discoveryType == 7:
            disType = 2 
        if discoveryType == 8:
            disType = 3
        if discoveryType == 9:
            disType = 4
        graphsPlans["plans"] = planner.backwardPlanningHeuristic(jobId, jsonRequest,plans,disType,similarityThreshold,feedbackThreshold,topK,functionalThreshold)
        feedbackManager.storePlans(graphsPlans,jobId)
    elif discoveryType <= 14:
        disType = 0
        if discoveryType == 10:
            disType = 0
        if discoveryType == 11:
            disType = 1
        if discoveryType == 12:
            disType = 2 
        if discoveryType == 13:
            disType = 3
        if discoveryType == 14:
            disType = 4
        solvedReq = planner.conversationApproach(jsonRequest,disType,similarityThreshold)
        jsonResponse["solvedReq"] = solvedReq 
        jsonResponse["requestTime"] = requestTime
        solvedTasks = solvedReq["tasks"]
        if len(solvedTasks) > 0:
            sol = True
            for task in solvedTasks:
                servicesTasks = task["services"]
                if len(servicesTasks) == 0:
                    sol = False
                    break
            if sol:
                pls = {}
                pl = PlanGraph(1, {}, [], 0.0, [], [], [])
                pls[len(pls)] = pl
                graphsPlans["plans"] = pls 
            else:
                pls = {}
                pl = PlanGraph(0, {}, [], 0.0, [], [], [])
                pls[len(pls)] = pl
                graphsPlans["plans"] = pls   
    else:
        feedbackThreshold = jsonRequest["feedbackThreshold"]
        functionalThreshold = jsonRequest["functionalThreshold"]
        topK = jsonRequest["topK"]
        disType = 0
        if discoveryType == 15:
            disType = 0
        if discoveryType == 16:
            disType = 1
        if discoveryType == 17:
            disType = 2 
        if discoveryType == 18:
            disType = 3
        if discoveryType == 19:
            disType = 4
        graphsPlans["plans"] = planner.backwardPlanningHeuristicACO(jobId, jsonRequest,plans,disType,similarityThreshold,feedbackThreshold,topK,functionalThreshold)
        feedbackManager.storePlans(graphsPlans,jobId)
    discoveryFinishTime = int(round(time.time() * 1000))
    discoveryTime = discoveryFinishTime - discoveryStartTime
    jsonRequest["discoveryTime"] = jsonRequest["discoveryTime"] + discoveryTime
    jsonResponse["jsonRequest"] = jsonRequest
    jsonResponse["jobId"] = jobId
    plans = [] 
    plans = planner.graphsToJsonPlans(graphsPlans,jobId)
    jsonResponse["jsonPlans"] = plans
    jsonResponse["gatewayAddress"] = gatewayAddress
    """ Respond if request solved, forward if not """
    if(len(plans)>0):
        solved = False
        for plan in plans:
            if plan["state"] == 1:
                solved = True
                break
            else:
                solved = False
        if solved:
            if discoveryType >=5 and discoveryType <= 9:
                print("Waiting for feedback: /discovery/feedback/"+jobId)
                subscribeToMessage(consumer,"/discovery/feedback/"+jobId)
            else:
                print("\n***** Waiting for new request *****")
                subscribeToMessage(gatewayAddress,"/discovery/request")
            jsonResponse["messageType"] = "response"
            jsonResponse["solved"] = 1
            message = json.dumps(jsonResponse)
            publishMessage(consumer,port,"/discovery/response/"+jobId,message)
        else:
            if lastGateway == False:
                if discoveryType >=5 and discoveryType <= 9:
                    print("Waiting for feedback: /discovery/feedback/"+jobId)
                    subscribeToMessage(consumer,"/discovery/feedback/"+jobId)
                else:
                    print("\n***** Waiting for new request *****")
                    subscribeToMessage(gatewayAddress,"/discovery/request")
                print("Forwarding Request with partial solutions: " + jobId)
                jsonResponse["messageType"] = "forward"
                message = json.dumps(jsonResponse)
                publishMessage(nextAddress,port,"/discovery/request",message)
            else:
                if discoveryType >=5 and discoveryType <= 9:
                    print("Waiting for feedback: /discovery/feedback/"+jobId)
                    subscribeToMessage(consumer,"/discovery/feedback/"+jobId)
                else:
                    print("\n***** Waiting for new request *****")
                    subscribeToMessage(gatewayAddress,"/discovery/request")
                print("Sending partial solutions Response: " + jobId)
                jsonResponse["messageType"] = "response"
                jsonResponse["solved"] = 0
                message = json.dumps(jsonResponse)
                publishMessage(consumer,port,"/discovery/response/"+jobId,message)
    else:
        if lastGateway == False:
            if (discoveryType >=5 and discoveryType <= 9) or (discoveryType >=15 and discoveryType <= 19):
                print("Waiting for feedback: /discovery/feedback/"+jobId)
                subscribeToMessage(consumer,"/discovery/feedback/"+jobId)
            else:
                print("\n***** Waiting for new request *****")
                subscribeToMessage(gatewayAddress,"/discovery/request")
            print("Forwarding Request: " + jobId)
            jsonResponse["messageType"] = "forward"
            message = json.dumps(jsonResponse)
            publishMessage(nextAddress,port,"/discovery/request",message)
        else:
            if discoveryType >=5 and discoveryType <= 9 or (discoveryType >=15 and discoveryType <= 19):
                print("Waiting for feedback: /discovery/feedback/"+jobId)
                subscribeToMessage(consumer,"/discovery/feedback/"+jobId)
            else:
                print("\n***** Waiting for new request *****")
                subscribeToMessage(gatewayAddress,"/discovery/request")
            print("Empty Response: " + jobId)
            jsonResponse["messageType"] = "response"
            jsonResponse["jsonPlans"] = {}
            message = json.dumps(jsonResponse)
            publishMessage(consumer,port,"/discovery/response/"+jobId,message)
    del plans
    del graphsPlans
def main():
    #dataGenerator.createConversationsRequests()
    """"i = 2
    while i<=10:
        dataGenerator.selectComposedQueries(i,15)
        i = i + 1"""
    
    """ Subscribe to request messages MQTT broker"""  
    print("\n***** Waiting for new request *****")
    subscribeToMessage(gatewayAddress,"/discovery/request")
    
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        print ("exiting")
    """requests = {}
    folders = os.listdir("../../Data/requests-dataset/json-requests/")
    req = 1
    i = 2
    while i < 6:
        files = os.listdir("../../Data/requests-dataset/json-requests/"+str(i)+"/")
        for file in files:
            request = {}
            with open("../../Data/requests-dataset/json-requests/" + str(i) + "/" + file) as dataFile:    
                request = json.load(dataFile)
            request["services"] = i
            plans = {}
            graphsPlans = {}
            planLength = i
            print("Folder: " + str(i))
            graphsPlans["plans"] = planner.backwardPlanningHeuristic2("1", request, plans, 3, 1.0,1.0,20,1.0, planLength)
            graphsPlans["plans"] = planner.backwardPlanningClassic2(request, plans, 3, 1.0,planLength)
            ps = graphsPlans["plans"]
            ps = planner.calculateMarks(ps)
            ps = planner.rankPlans(ps)
            ps = planner.topK(ps,50)
            jsonResponse={}
            jsonResponse["request"] = request
            graphsPlans["plans"]=ps
            ps = planner.graphsToJsonPlans(graphsPlans,"jobId001")
            jsonResponse["jsonPlans"] = ps
            with open("../../Data/requests-dataset/json-plans/" + str(i) + "/responses_" + file, 'w') as f:
                json.dump(jsonResponse, f, indent = 3)
        i = i + 1
        for folder in folders:
        files = os.listdir("../../Data/requests-dataset/json-requests/"+folder+"/")
        for file in files:
            request = {}
            with open("../../Data/requests-dataset/json-requests/" + folder + "/" + file) as dataFile:    
                request = json.load(dataFile)
            request["services"] = folder
            plans = {}
            graphsPlans = {}
            planLength = int(folder)
            print("Folder: " + folder)
            graphsPlans["plans"] = planner.backwardPlanningHeuristic2("1", request, plans, 3, 1.0,1.0,20,1.0, planLength)
            graphsPlans["plans"] = planner.backwardPlanningClassic2(request, plans, 3, 1.0,planLength)
            ps = graphsPlans["plans"]"""
      
    
if __name__== "__main__":
    main()
