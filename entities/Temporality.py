# *
# * Author: Christian Cabrera
# * Class that represents the temporality of a service
# 
class Temporality(object):
    def __init__(self, start, duration, unit, negotiable, negotiationConstraints):
        self.setStart(start)
        self.setDuration(duration)
        self.setUnit(unit)
        self.setNegotiable(negotiable)
        self.setNegotiationConstraints(negotiationConstraints)

    def getStart(self):
        return self._start

    def setStart(self, start):
        self._start = start

    def getDuration(self):
        return self._duration

    def setDuration(self, duration):
        self._duration = duration

    def getNegotiable(self):
        return self._negotiable

    def setNegotiable(self, negotiable):
        self._negotiable = negotiable

    def getUnit(self):
        return self._unit

    def setUnit(self, unit):
        self._unit = unit

    def getNegotiationConstraints(self):
        return self._negotiationConstraints

    def setNegotiationConstraints(self, negotiationConstraints):
        self._negotiationConstraints = negotiationConstraints