# *
# * Author: Christian Cabrera
# * Class that represents a vertice in a graph
# 
class Vertex(object):
    
    def __init__(self, type = -1, name="", state = 0, service={}, inputs=[], remainingInputs=[], outputs=[], domains=[]):
        self.reset()
        self.setType(type)
        self.setName(name)
        self.setState(0)
        self.setTask("")
        self.setService(service)
        self.setInputs(inputs)
        self.setRemainingInputs(remainingInputs)
        self.setOutputs(outputs)
        self.setDomains(domains)
    
    def reset(self):
        self.setType(-1)
        self.setName("")
        self.setState(0)
        self.setTask("")
        self.setService({})
        self.setInputs([])
        self.setRemainingInputs([])
        self.setOutputs([])
        self.setDomains([])

    def getType(self):
        return self._type

    def setType(self, type):
        self._type = type
    
    def getName(self):
        return self._name

    def setName(self, name):
        self._name = name
        
    def getState(self):
        return self._state

    def setState(self, state):
        self._state = state
        
    def getTask(self):
        return self._task

    def setTask(self, task):
        self._task = task
        
    def getService(self):
        return self._service
    
    def setService(self, service):
        self._service = service
        
    def getInputs(self):
        return self._inputs
    
    def setInputs(self, inputs):
        self._inputs = inputs
        
    def setRemainingInputs(self, remainingInputs):
        self._remainingInputs = remainingInputs
    
    def getRemainingInputs(self):
        return self._remainingInputs
    
    def getOutputs(self):
        return self._outputs
    
    def setOutputs(self, outputs):
        self._outputs = outputs
        
    def getDomains(self):
        return self._domains
    
    def setDomains(self, domains):
        self._domains = domains
     
    def delRemainingInput(self, input):
        while input in self._remainingInputs:
            self._remainingInputs.remove(input);   
        
        
        