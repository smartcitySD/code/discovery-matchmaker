# *
# * Author: Christian Cabrera
# * Class that represents the type of a service
# 
class Type(object):
    def __init__(self, physicalProcess, direction, dataType, unit):
        self.setPhysicalProcess(physicalProcess)
        self.setDirection(direction)
        self.setDataType(dataType)
        self.setUnit(unit)

    def getPhysicalProcess(self):
        return self._physicalProcess

    def setPhysicalProcess(self, physicalProcess):
        self._physicalProcess = physicalProcess

    def getDirection(self):
        return self._direction

    def setDirection(self, direction):
        self._direction = direction

    def getDataType(self):
        return self._dataType

    def setDataType(self, dataType):
        self._dataType = dataType

    def getUnit(self):
        return self._unit

    def setUnit(self, unit):
        self._unit = unit