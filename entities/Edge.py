# *
# * Author: Christian Cabrera
# * Class that represents an edge in a graph
# 

from entities.Vertex import Vertex

class Edge(object):
    
    def __init__(self, source = Vertex(-1,"",{},[],[],[]), destination = Vertex(-1,"",{},[],[],[]), link={}, degree=0.0):
        self.reset()
        self.setSource(source)
        self.setDestination(destination)
        self.setLink(link)
        self.setDegree(degree)
        self.setPheromone(0.0)
        self.setACOScore(0.0)
            
    def reset(self):
        self.setSource(Vertex(-1,"",{},[],[],[]))
        self.setDestination(Vertex(-1,"",{},[],[],[]))
        self.setLink({})
        self.setDegree(0)
        self.setDegree(0.0)
        self.setACOScore(0.0)

    def getSource(self):
        return self._source;
    
    def setSource(self,source):
        self._source = source
        
    def getDestination(self):
        return self._destination
    
    def setDestination(self, destination):
        self._destination = destination
        
    def getLink(self):
        return self._link
    
    def setLink(self, link):
        self._link = link
        
    def getDegree(self):
        return self._degree
    
    def setDegree(self, degree):
        self._degree = degree
        
    def getPheromone(self):
        return self._pheromone
    
    def setPheromone(self, pheromone):
        self._pheromone = pheromone
    
    def getACOScore(self):
        return self._ACOScore
    
    def setACOScore(self, ACOScore):
        self._ACOScore = ACOScore