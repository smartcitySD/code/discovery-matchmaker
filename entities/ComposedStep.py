# * Author: Christian Cabrera
# * This class represents a sequential step in a plan
# 
from entities.Step import Step

class ComposedStep(Step):
    
    def getSteps(self):
        return self._steps

    def setSteps(self, steps):
        self._steps = steps