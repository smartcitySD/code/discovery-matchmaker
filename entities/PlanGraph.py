# *
# * Author: Christian Cabrera
# * Class that represents an edge in a graph
# 

import copy

class PlanGraph(object):
    
    def __init__(self, state = 0, vertexs = {}, edges = [], mark = 0.0, inputs = [], currentInputs = [], outputs=[], domains = [], services=0):
        self.reset()
        self.setState(state)
        self.setVertexs(vertexs)
        self.setEdges(edges)
        self.setMark(mark)
        self.setInputs(inputs)
        self.setCurrentInputs(currentInputs)
        self.setOutputs(outputs)
        self.setDomains(domains)
        self.setServices(services)
        self.setACOProbability(0.0)
            
    def reset(self):
        self.setState(-1)
        self.setVertexs({})
        self.setEdges([])
        self.setMark(0.0)
        self.setInputs([])
        self.setCurrentInputs([])
        self.setOutputs([])
        self.setDomains([])
        self.setServices(0)
        self.setACOProbability(0.0)

    def getState(self):
        return self._state;
    
    def setState(self,state):
        self._state = state
        
    def getVertexs(self):
        return self._vertexs
    
    def setVertexs(self, vertexs):
        self._vertexs = vertexs
    
    def getEdges(self):
        return self._edges
    
    def setEdges(self, edges):
        self._edges = edges
        
    def getMark(self):
        return self._mark
    
    def setMark(self, mark):
        self._mark = mark
        
    def getInputs(self):
        return self._inputs
    
    def setInputs(self, inputs):
        self._inputs = inputs
    
    def getCurrentInputs(self):
        return self._currentInputs
    
    def setCurrentInputs(self, currentInputs):
        self._currentInputs = currentInputs

    def getOutputs(self):
        return self._outputs
    
    def setOutputs(self, outputs):
        self._outputs = outputs
        
    def getDomains(self):
        return self._domains
    
    def setDomains(self, domains):
        self._domains = domains
    
    def getACOProbability(self):
        return self._ACOProbability
    
    def setACOProbability(self, ACOProbability):
        self._ACOProbability = ACOProbability
    
    def addVertex(self, vertex):
        self._vertexs.append(vertex)
        
    def addEdge(self, edge):
        self._edges.append(edge)
        
    def getServices(self):
        return self._services

    def setServices(self, services):
        self._services = services
        
    def getRemainingVertexs(self):
        remainingVertexs = {}
        for k, vertex in self.getVertexs().items():
            if vertex.getState() == 0:
                remainingVertexs[k] = vertex 
        return remainingVertexs
    
    def update(self,vertex,discoveredVertex,input,output):
        inp = copy.deepcopy(input)
        if "state" in inp.keys():
            del inp["state"]
        for k, v in self.getRemainingVertexs().items():
            if vertex.getName() == v.getName():
                v.delRemainingInput(inp)
            if len(v.getRemainingInputs()) == 0:
                v.setState(1)
        
        if discoveredVertex.getType() != 0:
            for i in discoveredVertex.getInputs():
                if i not in self.getCurrentInputs(): 
                    self.getCurrentInputs().append(i)
        
        
        remainingInputs = []
        for k, v in self.getRemainingVertexs().items():
            for i in v.getRemainingInputs():
                remainingInputs.append(i)
        if inp not in remainingInputs:
            while inp in self.getCurrentInputs():
                self.getCurrentInputs().remove(inp);
                
        if len(self.getCurrentInputs()) == 0:
            self.setState(1)
    
    
        