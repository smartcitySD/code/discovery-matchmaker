# *
# * Author: Christian Cabrera
# * Class that represents the sampling period of a service
# 
class SamplingPeriod(object):
    def __init__(self, value, description, unit, negotiable, negotiationConstraints):
        self.setValue(value)
        self.setDescription(description)
        self.setUnit(unit)
        self.setNegotiable(negotiable)
        self.setNegotiationConstraints(negotiationConstraints)

    def getValue(self):
        return self._value

    def setValue(self, value):
        self._value = value

    def getDescription(self):
        return self._description

    def setDescription(self, description):
        self._description = description

    def getUnit(self):
        return self._unit

    def setUnit(self, unit):
        self._unit = unit

    def getNegotiable(self):
        return self._negotiable

    def setNegotiable(self, negotiable):
        self._negotiable = negotiable

    def getNegotiationConstraints(self):
        return self._negotiationConstraints

    def setNegotiationConstraints(self, negotiationConstraints):
        self._negotiationConstraints = negotiationConstraints